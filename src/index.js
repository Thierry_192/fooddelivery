import React from "react"
import { Image } from "react-native"
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/FontAwesome';

//screens
import HomeScreen from "./screens/home"
import CartScreen from "./screens/cart"
import AdressScreen from "./screens/address"
import ProfileScreen from "./screens/profile"

const Tab = createMaterialBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator 
        initialRouteName="Home"
        activeColor="black"
        inactiveColor="#fafafa"
        barStyle={{ backgroundColor: '#c6d065' }}
      >
        <Tab.Screen name="Menu" component={HomeScreen} options={{tabBarIcon: ({ color }) => (
          <Image style={{ height: 30, width: 30, top: -4}} source={require("./assets/food.png")}/>
        )}}/>
        <Tab.Screen name="Endereço" component={AdressScreen} options={{tabBarIcon: ({ color }) => (
          <Image style={{ height: 30, width: 30, top: -4}} source={require("./assets/address.png")}/>
        )}}/>
        <Tab.Screen name="Carrinho" component={CartScreen} options={{tabBarIcon: ({ color }) => (
          <Image style={{ height: 30, width: 30, top: -4}} source={require("./assets/cart.png")}/>
        )}}/>
        <Tab.Screen name="Perfil" component={ProfileScreen} options={{tabBarIcon: ({ color }) => (
          <Image style={{ height: 30, width: 30, top: -4}} source={require("./assets/profile.png")}/>
        )}}/>
      </Tab.Navigator>
    </NavigationContainer>
  );
}