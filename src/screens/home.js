import React, { Component } from 'react';
import { 
  Text,
  Image,
  StyleSheet,
  Dimensions,
  View,
  ScrollView,
  TextInput,
  FlatList,
  TouchableOpacity,
  TouchableHighlight,
  _Text,
} from 'react-native';
import Swiper from 'react-native-swiper'
import AsyncStorage from '@react-native-community/async-storage';

var {height, width } = Dimensions.get('window');
console.disableYellowBox = true;

  export default class App extends Component {

    constructor(props) {
      super(props)
      
      this.state = {
        dataBanner: [],
        dataCategories: [],
        selectCatg: 0,
        dataFood: []
      }
    }

    componentDidMount(){
      const url = "http://tutofox.com/foodapp/api.json"
      return fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
  
        this.setState({
          isLoading: false,
          dataBanner: responseJson.banner,
          dataCategories: responseJson.categories,
          dataFood: responseJson.food,
        });
  
      })
      .catch((error) =>{
        console.error(error);
      });
    }

    renderCategories(item) {
      return(
        <TouchableOpacity style={[styles.divCategorie, {backgroundColor: item.color}]}
          onPress={() => this.setState({selectCatg: item.id})}
          >
          <Image source={{uri: item.image}} style={{width:100,height:80}} resizeMode="contain"/>
          <Text style={styles.textCategorieName}>{item.name}</Text>
        </TouchableOpacity>
      )
    }

    onClickAddCart(data) {
      const itemCart = {
        food: data,
        quantity: 1,
        price: data.price
      }

      AsyncStorage.getItem('cart').then(dataCart => {
        if (dataCart) {
          const cart = JSON.parse(dataCart);
          cart.push(itemCart);
          AsyncStorage.setItem('cart', JSON.stringify(cart))
        } else {
          let cart = []
          AsyncStorage.setItem('cart', JSON.stringify(cart))
        }
        alert("Adicionado com sucesso")
      }).catch(error => alert("Ops", "Houve algum erro"))
    }

    renderFood(item) {
      let catg = this.state.selectCatg;
      if (catg == 0 || catg == item.categorie) {
        return (
          <TouchableOpacity style={styles.divFood}>
            <Image 
              style={styles.imageFood}
              resizeMode="contain"
              source={{uri: item.image}}
            />
            <View style={{height: ((width/2)-20)-90, width: ((width/2)-20)-10, backgroundColor: 'transparent'}}/>
            <Text style={{fontWeight: 'bold', fontSize: 22, textAlign: 'center'}}> {item.name} </Text>
            <Text> Descp and Details </Text>
            <Text style={{fontSize: 20, color: 'green', textAlign: 'center'}}>${item.price}</Text>
            <TouchableOpacity
              onPress={() => this.onClickAddCart(item)} 
              style={{  
                width:(width/2)-40,
                backgroundColor:'#33c37d',
                flexDirection:'row',
                alignItems:'center',
                justifyContent:"center",
                borderRadius:5,
                padding:4
              }} 
              >
              <Text style={{fontSize: 18, color: "white", fontWeight:'bold'}}> Comprar <Image source={require("../assets/add.png")}/></Text>
            </TouchableOpacity>
          </TouchableOpacity>
        );
      }
    }

    render() {
      return (
        <ScrollView>
          <View style={{flex: 1, backgroundColor: "#f2f2f2"}}>
            <View style={{width: width, alignItems:'center'}}>
              <Image style={{height:60, width:width/2, margin:10}} resizeMode="contain" source={{uri: "https://tutofox.com/foodapp/foodapp.png"}}/>
                <Swiper style={{height: width/2}} loop={true} autoplay={true} autoplayTimeout={2} >
                  {this.state.dataBanner.map(banner => {
                    return (
                      <Image style={styles.imageBanner} source={{uri:banner}} resizeMode="contain"/>
                    )
                  })}
                </Swiper>
                <View style={{height:20}} />
            </View>

          </View>
          <View style={styles.containerFlatList}>
            <FlatList 
              horizontal={true}
              data={this.state.dataCategories}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item}) => this.renderCategories(item)}
            />
            <FlatList 
              numColumns={'2'}
              data={this.state.dataFood}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item}) => (this.renderFood(item))}  
            />
          </View>

        </ScrollView> 
      );
    }
  }

  const styles = StyleSheet.create({
    imageBanner: {
      height: width/2,
      width: width-40,
      borderRadius: 10,
      marginHorizontal: 20
    }, 
    divCategorie: {
      backgroundColor: "red",
      margin:5, 
      alignItems:'center',
      borderRadius:10,
      padding:10
    }, 
    textCategorieName: {
      fontWeight:'bold',
      fontSize:22
    },
    titleCatg: {
      fontSize: 30,
      fontWeight: 'bold',
      textAlign: "center",
      marginBottom: 10
    },
    imageFood: {
      width: ((width/2)-20)-10,
      height: ((width/2)-20)-30,
      backgroundColor: 'transparent',
      position:'absolute',
      top: -45,
    },
    divFood: {
      width: (width/2-20),
      padding: 10,
      borderRadius: 10,
      marginBottom: 5,
      marginTop: 55,
      marginLeft: 10,
      alignItems: 'center',
      elevation: 8,
      shadowOpacity: 0.3,
      shadowRadius: 50,
      backgroundColor: "white",
    }
  })
